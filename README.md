# My-Deferred
A tiny library to help with promises you can check and resolve from outside

## Installation

Just add the npm package to your project with

```sh
npm install -S my-deferred
```

## Usage

This is how you create a deferred and resolve it later. 

```ts
const def = new Deferred<string>();
// def.val will now return null;

def.promise.then((val) => {
  console.log(`Promise has been resolved with ${val}!`)
})

console.log(def.isPending())
// prints true

console.log(def.isResolved())
// prints false

def.resolve("Hello World");
// will now output "Promise has been resolved with Hello World!"
```

you can also reject the deferred using

```ts

const def = new Deferred<string>();

def.promise
  .then(() => console.log("won't happen"))
  .catch(err => console.log(err))

def.reject({
  message: "something has gone wrong!"
});
// Will now print out "somehting has gone wrong!"
```