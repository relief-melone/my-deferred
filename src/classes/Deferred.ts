export default class Deferred<T> {
  public promise: Promise<T>;

  private fate: 'resolved' | 'unresolved';
  private state: 'pending' | 'fulfilled' | 'rejected';
  private _val: T | null;

  private _resolve!: (val: T) => void;
  private _reject!: (val: T) => void;

  constructor(defaultVal:T | null = null) {
    this.state = 'pending';
    this.fate = 'unresolved';
    this._val = defaultVal || null;
    this.promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
      this._reject = reject;
    });
    this.promise.then(
      (val:T) => {
        this.state = 'fulfilled';
        this._val = val;
      },
      () => {
        this.state = 'rejected';
        this._val = null;
      }
    );
  }

  resolve(value?: any) {
    if (this.fate === 'resolved') {
      throw 'Deferred cannot be resolved twice';
    }
    this.fate = 'resolved';
    this._resolve(value);
  }

  reject(reason?: any) {
    if (this.fate === 'resolved') {
      throw 'Deferred cannot be resolved twice';
    }
    this.fate = 'resolved';
    this._reject(reason);
  }

  isResolved() {
    return this.fate === 'resolved';
  }

  isPending() {
    return this.state === 'pending';
  }

  isFulfilled() {
    return this.state === 'fulfilled';
  }

  isRejected() {
    return this.state === 'rejected';
  }

  get val():T|null{
    return this._val;
  }
}