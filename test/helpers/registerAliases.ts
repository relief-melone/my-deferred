import moduleAlias from 'module-alias';
console.log('REGISTERING ALIASSES FOR TESTS');


moduleAlias.addAliases({
  '@': `${__dirname}/../../src`,
  '@test': `${__dirname}/../../test`,
  '@root': `${__dirname}/../../`,
});
