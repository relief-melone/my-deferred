import Deferred from '@/index';
import { expect } from 'chai';

describe('my-deferred', () => {
  it('will implement the correct initial state', () => {
    // Prepare
    const deferred = new Deferred<boolean>();
    
    // Execute
    
    // Assert
    expect(deferred.isFulfilled()).to.equal(false);
    expect(deferred.isPending()).to.equal(true);
    expect(deferred.isRejected()).to.equal(false);
    expect(deferred.isResolved()).to.equal(false);
    expect(deferred.val).to.equal(null);


  });

  it('will correctly resolve', async () => {
    // Prepare
    const deferred = new Deferred<boolean>();
    
    // Execute
    deferred.resolve(true);
    
    // Assert
    const val = await deferred.promise;
    expect(deferred.isFulfilled()).to.equal(true);
    expect(deferred.isPending()).to.equal(false);
    expect(deferred.isRejected()).to.equal(false);
    expect(deferred.isResolved()).to.equal(true);
    expect(val).to.equal(true);
  });

  it('will correctly be rejected', async () => {
    // Prepare
    const deferred = new Deferred<boolean>();
    
    // Execute
    deferred.reject(false);
    
    try {
      await deferred.promise;
    } catch (err) {
      // Assert
      expect(deferred.isFulfilled()).to.equal(false);
      expect(deferred.isPending()).to.equal(false);
      expect(deferred.isRejected()).to.equal(true);
      expect(deferred.isResolved()).to.equal(true);
      expect(err).to.equal(false);
    }
    
  });

  it('sould correctly have a default value', () => {
    // Prepare
    const deferred = new Deferred<string>('Hello World!');

    // Assert
    expect(deferred.val).to.equal('Hello World!');

  });
});